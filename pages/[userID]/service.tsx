import { CalendarIcon, MapPinIcon, PlusIcon } from "@heroicons/react/20/solid";
import { useEffect, useState } from "react";
import Container from "../../components/Container";
import Header from "../../components/header";

const positions = [
  {
    id: 1,
    title: "Helping Elders",
    city: "Seattle",
    state: "WA",
    zip: "98101",
    startDate: "2020-01-07",
    startDateFull: "January 7, 2020",
  },
  {
    id: 2,
    title: "Helping Elders",
    city: "Seattle",
    state: "WA",
    zip: "98101",
    startDate: "2020-01-07",
    startDateFull: "January 7, 2020",
  },
  {
    id: 3,
    title: "Helping Elders",
    city: "Seattle",
    state: "WA",
    zip: "98101",
    startDate: "2020-01-07",
    startDateFull: "January 7, 2020",
  },
  {
    id: 4,
    title: "Helping Elders",
    city: "Seattle",
    state: "WA",
    zip: "98101",
    startDate: "2020-01-07",
    startDateFull: "January 7, 2020",
  },
  {
    id: 5,
    title: "Helping Elders",
    city: "Seattle",
    state: "WA",
    zip: "98101",
    startDate: "2020-01-07",
    startDateFull: "January 7, 2020",
  },
  {
    id: 6,
    title: "Helping Elders",
    city: "Seattle",
    state: "WA",
    zip: "98101",
    startDate: "2020-01-07",
    startDateFull: "January 7, 2020",
  },
  {
    id: 7,
    title: "Trash Pickup",
    city: "Provo",
    state: "UT",
    zip: "84606",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 8,
    title: "Trash Pickup",
    city: "Provo",
    state: "UT",
    zip: "84606",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 9,
    title: "Trash Pickup",
    city: "Provo",
    state: "UT",
    zip: "84606",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 10,
    title: "Trash Pickup",
    city: "Provo",
    state: "UT",
    zip: "84606",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 11,
    title: "Trash Pickup",
    city: "Provo",
    state: "UT",
    zip: "84606",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 12,
    title: "Trash Pickup",
    city: "Provo",
    state: "UT",
    zip: "84606",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 13,
    title: "Cancer Fundraiser",
    city: "New York",
    state: "NY",
    zip: "10001",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 14,
    title: "Cancer Fundraiser",
    city: "New York",
    state: "NY",
    zip: "10001",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 15,
    title: "Cancer Fundraiser",
    city: "New York",
    state: "NY",
    zip: "10001",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 16,
    title: "Cancer Fundraiser",
    city: "New York",
    state: "NY",
    zip: "10001",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 17,
    title: "Cancer Fundraiser",
    city: "New York",
    state: "NY",
    zip: "10001",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 18,
    title: "Cancer Fundraiser",
    city: "New York",
    state: "NY",
    zip: "10001",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 19,
    title: "Walk for Hunger",
    city: "Salt Lake City",
    state: "UT",
    zip: "84101",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 20,
    title: "Walk for Hunger",
    city: "Salt Lake City",
    state: "UT",
    zip: "84101",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 21,
    title: "Walk for Hunger",
    city: "Salt Lake City",
    state: "UT",
    zip: "84101",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 22,
    title: "Walk for Hunger",
    city: "Salt Lake City",
    state: "UT",
    zip: "84101",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 23,
    title: "Walk for Hunger",
    city: "Salt Lake City",
    state: "UT",
    zip: "84101",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
  {
    id: 24,
    title: "Walk for Hunger",
    city: "Salt Lake City",
    state: "UT",
    zip: "84101",
    startDate: "2022-10-22",
    startDateFull: "October 22, 2022",
  },
];

export default function Service() {
  const [search, setSearch] = useState("");
  const [filteredPositions, setFilteredPositions] = useState(positions);

  const handleSubmit = (e) => {
    console.log("e: ", e);
    setSearch(e.target.zip);
  };

  // when the user changes the filteredPositions, the page should re-render
  useEffect(() => {
    console.log("search", search);
    let filteredPositions = positions;
    filteredPositions = filteredPositions.filter((position) => {
      return position.zip.includes(search);
    });
    setFilteredPositions(filteredPositions);
    console.log(filteredPositions);
  }, [search]);

  return (
    <Container>
      <main className="flex-1">
        <div className="px-10">
          <Header
            title="Service"
            description="Look for service opportunities in your area."
          />
          <div className="flex flex-col justify-center items-center">
            <form
              onSubmit={handleSubmit}
              className="mt-12 sm:w-full sm:max-w-lg"
            >
              <div className="min-w-0 flex-1">
                <label htmlFor="hero-email" className="sr-only">
                  Zip Code
                </label>
                <input
                  id="zip"
                  type="text"
                  className="block w-full rounded-md border border-gray-300 px-5 py-3 text-base text-gray-900 placeholder-gray-500 shadow-sm focus:border-indigo-500 focus:ring-indigo-500"
                  placeholder="Enter your zip code"
                  value={search}
                  onChange={(e) => setSearch(e.target.value)}
                />
              </div>
            </form>
          </div>
          <div className="my-12">
            {filteredPositions.length !== 0 ? (
              <Table filteredPositions={filteredPositions} />
            ) : (
              <Empty />
            )}
          </div>
        </div>
      </main>
    </Container>
  );
}

function Table({ filteredPositions }) {
  return (
    <div className="overflow-hidden bg-white shadow sm:rounded-md">
      <ul role="list" className="divide-y divide-gray-200">
        {filteredPositions.map((position) => (
          <li key={position.id}>
            <a href="#" className="block hover:bg-gray-50">
              <div className="px-4 py-4 sm:px-6">
                <div className="flex items-center justify-between">
                  <p className="truncate text-sm font-medium text-indigo-600">
                    {position.title}
                  </p>
                </div>
                <div className="mt-2 sm:flex sm:justify-between">
                  <div className="sm:flex">
                    <p className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0">
                      <MapPinIcon
                        className="mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400"
                        aria-hidden="true"
                      />
                      {position.city}, {position.state} {position.zip}
                    </p>
                  </div>
                  <div className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0">
                    <CalendarIcon
                      className="mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400"
                      aria-hidden="true"
                    />
                    <p>
                      <time dateTime={position.startDate}>
                        {position.startDateFull}
                      </time>
                    </p>
                  </div>
                </div>
              </div>
            </a>
          </li>
        ))}
      </ul>
      {/* <nav
        className="flex items-center justify-between border-t border-gray-200 bg-white px-4 py-3 sm:px-6"
        aria-label="Pagination"
      >
        <div className="hidden sm:block">
          <p className="text-sm text-gray-700">
            Showing <span className="font-medium">1</span> to{" "}
            <span className="font-medium">10</span> of{" "}
            <span className="font-medium">{filteredPositions.length}</span>{" "}
            results
          </p>
        </div>
        <div className="flex flex-1 justify-between sm:justify-end">
          <a
            className="relative inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50"
          >
            Previous
          </a>
          <a className="relative ml-3 inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50">
            Next
          </a>
        </div>
      </nav> */}
    </div>
  );
}

function Empty() {
  return (
    <div className="text-center">
      <svg
        className="mx-auto h-12 w-12 text-gray-400"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
        aria-hidden="true"
      >
        <path
          vectorEffect="non-scaling-stroke"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M9 13h6m-3-3v6m-9 1V7a2 2 0 012-2h6l2 2h6a2 2 0 012 2v8a2 2 0 01-2 2H5a2 2 0 01-2-2z"
        />
      </svg>
      <h3 className="mt-2 text-sm font-medium text-gray-900">
        No Service Opportunities
      </h3>
      <p className="mt-1 text-sm text-gray-500">Try a different zip code</p>
    </div>
  );
}
