import Container from "../../components/Container";

export default function Shadowing() {
  return (
    <Container>
      <h1 className="text-3xl font-bold underline">Shadowing Page</h1>
    </Container>
  );
}
