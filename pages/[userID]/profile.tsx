import Container from "../../components/Container";

export default function Profile() {
  return (
    <Container>
      <h1 className="text-3xl font-bold underline">Profile Page</h1>
    </Container>
  );
}
