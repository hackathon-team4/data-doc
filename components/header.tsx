export default function Header({ title, description }) {
  return (
    <div className="bg-white">
      <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:px-8">
        <h1 className="text-3xl font-bold">{title}</h1>
        <p className="mt-4 text-lg leading-6 text-gray-500">{description}</p>
      </div>
    </div>
  );
}
